package plugins.tprovoost.Microscopy.MicroManager.event;

public interface CoreListener
{
    public void onPropertiesChanged();

    public void onConfigGroupChanged(String groupName, String newConfig);

    public void onExposureChanged(String deviceName, double exposure);

    public void onPropertyChanged(String deviceName, String propName, String propValue);

    public void onStagePositionChanged(String deviceName, double pos);

    public void onXYStagePositionChanged(String deviceName, double xPos, double yPos);

    public void onSystemConfigurationLoaded();
}
