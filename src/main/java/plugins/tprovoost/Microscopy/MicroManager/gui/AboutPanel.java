package plugins.tprovoost.Microscopy.MicroManager.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.micromanager.MMVersion;

import icy.network.NetworkUtil;

public class AboutPanel extends JPanel
{
    /**
     * 
     */
    private static final long serialVersionUID = -7561207579670139391L;

    JButton OkBtn;

    /**
     * Create the panel.
     * 
     * @param OkAction
     *        action to execute on OK button click
     */
    public AboutPanel(ActionListener OkAction)
    {
        super();

        initialize();

        if (OkAction != null)
            OkBtn.addActionListener(OkAction);
    }

    private void initialize()
    {
        setBorder(new EmptyBorder(10, 10, 10, 10));
        JPanel center = new JPanel(new BorderLayout());
        final JLabel value = new JLabel("<html><body>"
                + "<h2>About</h2><h4>Micro-Manager for Icy is being developed by Stephane Dallongeville, Irsath Nguyen and Thomas Provoost."
                + "<br/>Copyright 2021, Institut Pasteur.</h4>" + "<p>This plugin is based on Micro-Manager "
                + MMVersion.VERSION_STRING + " which is developed under the following license:<br/>"
                + "<i>This software is distributed free of charge in the hope that it will be useful, but WITHOUT ANY WARRANTY;<br/>"
                + "without even the implied warranty of merchantability or fitness for a particular purpose.<br/>"
                + "In no event shall the copyright owner or contributors be liable for any direct, indirect, incidental spacial, examplary,<br/>"
                + "or consequential damages.<br/>"
                + "<b>Copyright University of California San Francisco, 2010. All rights reserved.</b></i><br/></p>"
                + "<br/>"
                + "When you publish results using Micro-Manager, please cite one of the following papers:<br/>"
                + "<i><a href=\"https://www.jbmethods.org/jbm/article/view/36/28\">(PDF)</a> Arthur D Edelstein, Mark A Tsuchida, Nenad Amodaj, Henry Pinkard, Ronald D Vale, and Nico Stuurman (2014),<br/>"
                + "Advanced methods of microscope control using μManager software. Journal of Biological Methods 2014 1(2):e11 <a href=\"http://dx.doi.org/10.14440/jbm.2014.36\">doi:10.14440/jbm.2014.36</a><br/>"
                + "<a href=\"http://valelab.ucsf.edu/publications/2010EdelsteinCurrProt.pdf\">(PDF)</a> Arthur Edelstein, Nenad Amodaj, Karl Hoover, Ron Vale, and Nico Stuurman (2010),<br/>"
                + "Computer Control of Microscopes Using μManager. Current Protocols in Molecular Biology 14.20.1-14.20.17 <a href=\"http://dx.doi.org/10.1002/0471142727.mb1420s92\">doi:10.1002/0471142727.mb1420s92</a><br/></i>"

                + "</body></html>");
        JLabel link = new JLabel("<html><a href=\"\">For more information, please follow this link.</a></html>");
        link.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mousePressed(MouseEvent mouseevent)
            {
                NetworkUtil.openBrowser("http://www.micro-manager.org/");
            }
        });
        value.setSize(new Dimension(50, 18));
        value.setAlignmentX(SwingConstants.HORIZONTAL);
        value.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));

        center.add(value, BorderLayout.CENTER);
        center.add(link, BorderLayout.SOUTH);

        JPanel panel_south = new JPanel();
        panel_south.setLayout(new BoxLayout(panel_south, BoxLayout.X_AXIS));
        OkBtn = new JButton("OK");
        panel_south.add(Box.createHorizontalGlue());
        panel_south.add(OkBtn);
        panel_south.add(Box.createHorizontalGlue());

        setLayout(new BorderLayout());
        add(center, BorderLayout.CENTER);
        add(panel_south, BorderLayout.SOUTH);
    }
}
